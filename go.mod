module burntworld.ca/terraform-provider-ganeti

go 1.15

require (
	burntworld.ca/go-rapi-client v1.0.0
	github.com/hashicorp/terraform-plugin-sdk/v2 v2.0.4
)

replace burntworld.ca/go-rapi-client => ../go-rapi-client

terraform {
  required_providers {
    ganeti = {
      versions = ["0.2"]
      source = "burntworld.ca/x/ganeti"
    }
  }
}

provider "ganeti" {}

data "ganeti_networks" "all" {}

output "all_networks" {
  value = data.ganeti_networks.all.networks
}


data "ganeti_instances" "all" {}
output "instance" {
  value = data.ganeti_instances.all.instances
}

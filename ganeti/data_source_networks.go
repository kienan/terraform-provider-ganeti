package ganeti

import (
	"context"
	"strconv"
	"time"

	rapi "burntworld.ca/go-rapi-client"
	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
)

func dataSourceNetworks() *schema.Resource {
	return &schema.Resource{
		ReadContext: dataSourceNetworksRead,
		Schema: map[string]*schema.Schema{
			"networks": &schema.Schema{
				Type:     schema.TypeList,
				Computed: true,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"name": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
						"ctime": &schema.Schema{
							Type:     schema.TypeFloat,
							Computed: true,
						},
						"external_reservations": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
						"free_count": &schema.Schema{
							Type:     schema.TypeInt,
							Computed: true,
						},
						"gateway": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
						"gateway6": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
						"group_list": &schema.Schema{
							Type:     schema.TypeList,
							Computed: true,
							Elem: &schema.Schema{
								Type: schema.TypeList,
								Elemb: &schema.Schema{
									Type: schema.TypeString,
								},
							},
						},
						"inst_list": &schema.Schema{
							Type:     schema.TypeList,
							Computed: true,
							Elem: &schema.Schema{
								Type: schema.TypeString,
							},
						},
						"mac_prefix": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
						"map": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
						"mtime": &schema.Schema{
							Type:     schema.TypeFloat,
							Computed: true,
						},
						"network": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
						"network6": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
						"reserved_count": &schema.Schema{
							Type:     schema.TypeInt,
							Computed: true,
						},
						"serial_no": &schema.Schema{
							Type:     schema.TypeInt,
							Computed: true,
						},
						"tags": &schema.Schema{
							Type:     schema.TypeList,
							Computed: true,
							Elem: &schema.Schema{
								Type: schema.TypeString,
							},
						},
						"uuid": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
					},
				},
			},
		},
	}
}

func dataSourceNetworksRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	client := m.(*rapi.Client)
	var diags diag.Diagnostics
	networks, err := client.GetNetworksBulk()
	if err != nil {
		return diag.FromErr(err)
	}

	if err = d.Set("networks", networks); err != nil {
		return diag.FromErr(err)
	}

	// always run
	d.SetId(strconv.FormatInt(time.Now().Unix(), 10))

	return diags
}

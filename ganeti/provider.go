package ganeti

import (
	"context"

	rapi "burntworld.ca/go-rapi-client"
	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
)

// Provider -
func Provider() *schema.Provider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"username": &schema.Schema{
				Type: schema.TypeString,
				Optional: true,
				DefaultFunc: schema.EnvDefaultFunc("GANETI_USERNAME", nil),
			},
			"password": &schema.Schema{
				Type: schema.TypeString,
				Optional: true,
				Sensitive: true,
				DefaultFunc: schema.EnvDefaultFunc("GANETI_PASSWORD", nil),
 			},
			"host": &schema.Schema{
				Type: schema.TypeString,
				Optional: true,
				DefaultFunc: schema.EnvDefaultFunc("GANETI_HOST", nil),
			},
			"port": &schema.Schema{
				Type: schema.TypeInt,
				Optional: true,
				DefaultFunc: schema.EnvDefaultFunc("GANETI_PORT", 5080),
			},
			"apiversion": &schema.Schema{
				Type: schema.TypeInt,
				Optional: true,
				DefaultFunc: schema.EnvDefaultFunc("GANETI_APIVERSION", 2),
			},
		},
		ConfigureContextFunc: providerConfigure,
		DataSourcesMap: map[string]*schema.Resource {
			"ganeti_networks": dataSourceNetworks(),
			"ganeti_instances": dataSourceInstance(),
			},
		ResourcesMap: map[string]*schema.Resource{},
	}
}

func providerConfigure(ctx context.Context, d *schema.ResourceData) (interface{}, diag.Diagnostics) {
	username := d.Get("username").(string)
	password := d.Get("password").(string)
	host := d.Get("host").(string)
	port := d.Get("port").(int)
	apiversion := d.Get("apiversion").(int)

	var diags diag.Diagnostics

	c := rapi.NewClient(username, password, host, port, apiversion)
	return c, diags
}

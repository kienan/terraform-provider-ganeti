package ganeti

import (
	"context"

	rapi "burntworld.ca/go-rapi-client"
	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
)

func dataSourceInstance() *schema.Resource {
	return &schema.Resource{
		ReadContext: dataSourceInstanceRead,
		Schema: map[string]*schema.Schema{
			"instances": &schema.Schema{
				Type:     schema.TypeList,
				Computed: true,
				Elem: &schema.Resource{
					Schema: map[string]*schema.Schema{
						"disk_usage": &schema.Schema{
							Type:     schema.TypeInt,
							Computed: true,
						},
						"oper_vcpus": &schema.Schema{
							Type:     schema.TypeInt,
							Computed: true,
						},
						"network_port": &schema.Schema{
							Type:     schema.TypeInt,
							Computed: true,
						},
						"serial_no": &schema.Schema{
							Type:     schema.TypeInt,
							Computed: true,
						},
						"ctime": &schema.Schema{
							Type:     schema.TypeFloat,
							Computed: true,
						},
						"mtime": &schema.Schema{
							Type:     schema.TypeFloat,
							Computed: true,
						},
						"oper_state": &schema.Schema{
							Type:     schema.TypeBool,
							Computed: true,
						},
						"disk_template": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
						"name": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
						"status": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
						"oper_ram": &schema.Schema{
							Type:     schema.TypeInt,
							Computed: true,
						},
						"pnode": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
						"admin_state": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
						"os": &schema.Schema{
							Type:     schema.TypeString,
							Computed: true,
						},
						"nic.uuids": &schema.Schema{
							Type:     schema.TypeList,
							Computed: true,
							Deprecated: "to satisfy terraform?",
							Elem: &schema.Schema{
								Type: schema.TypeString,
							},
						},
						"nic_modes": &schema.Schema{
							Type:     schema.TypeList,
							Computed: true,
							Elem: &schema.Schema{
								Type: schema.TypeString,
							},
						},
						"nic.names": &schema.Schema{
							Type:     schema.TypeList,
							Computed: true,
							Deprecated: "to satisfy terraform?",
							Elem: &schema.Schema{
								Type: schema.TypeString,
							},
						},
						"nic.networks.names": &schema.Schema{
							Type:     schema.TypeList,
							Computed: true,
							Deprecated: "to satisfy terraform?",
							Elem: &schema.Schema{
								Type: schema.TypeString,
							},
						},
						"disk.spindles": &schema.Schema{
							Type:     schema.TypeList,
							Computed: true,
							Deprecated: "to satisfy terraform?",
							Elem: &schema.Schema{
								Type: schema.TypeString,
							},
						},
						"disk.uuids": &schema.Schema{
							Type:     schema.TypeList,
							Computed: true,
							Deprecated: "to satisfy terraform?",
							Elem: &schema.Schema{
								Type: schema.TypeString,
							},
						},
						"disk.sizes": &schema.Schema{
							Type:     schema.TypeList,
							Computed: true,
							Deprecated: "to satisfy terraform?",
							Elem: &schema.Schema{
								Type: schema.TypeInt,
							},
						},
						"tags": &schema.Schema{
							Type:     schema.TypeList,
							Computed: true,
							Elem: &schema.Schema{
								Type: schema.TypeString,
							},
						},
						"nic.networks": &schema.Schema{
							Type:     schema.TypeList,
							Computed: true,
							Deprecated: "to satisfy terraform?",
							Elem: &schema.Schema{
								Type: schema.TypeString,
							},
						},
						"nic.macs": &schema.Schema{
							Type:     schema.TypeList,
							Computed: true,
							Deprecated: "to satisfy terraform?",
							Elem: &schema.Schema{
								Type: schema.TypeString
							},
						},
						"nic.bridges": &schema.Schema{
							Type:     schema.TypeList,
							Computed: true,
							Deprecated: "to satisfy terraform?",
							Elem: &schema.Schema{
								Type: schema.TypeString,
							},
						},
						"nic.ips": &schema.Schema{
							Type:     schema.TypeList,
							Computed: true,
							Deprecated: "to satisfy terraform",
							Elem: &schema.Schema{
								Type: schema.TypeString,
							},
						},
						"nic.links": &schema.Schema{
							Type:     schema.TypeList,
							Computed: true,
							Deprecated: "to satisfy terraform?",
							Elem: &schema.Schema{
								Type: schema.TypeString,
							},
						},
						"snodes": &schema.Schema{
							Type:     schema.TypeList,
							Computed: true,
							Elem: &schema.Schema{
								Type: schema.TypeString,
							},
						},
						"hvparams": &schema.Schema{
							Type:     schema.TypeMap,
							Computed: true,
							Elem: &schema.Schema{
								Type: schema.TypeString,
							},
						},
						"beparams": &schema.Schema{
							Type:     schema.TypeMap,
							Computed: true,
							Elem: &schema.Schema{
								Type: schema.TypeString,
							},
						},
						"custom_hvparams": &schema.Schema{
							Type:     schema.TypeMap,
							Computed: true,
							Elem: &schema.Schema{
								Type: schema.TypeString,
							},
						},
						"custom_beparams": &schema.Schema{
							Type:     schema.TypeMap,
							Computed: true,
							Elem: &schema.Schema{
								Type: schema.TypeString,
							},
						},
						"custom_nicparams": &schema.Schema{
							Type:     schema.TypeList,
							Computed: true,
							Elem: &schema.Schema{
								Type: schema.TypeMap,
								Elem: &schema.Schema{
									Type: schema.TypeString,
								},
							},
						},
					},
				},
			},
		},
	}
}

func dataSourceInstanceRead(ctx context.Context, d *schema.ResourceData, m interface{}) diag.Diagnostics {
	var diags diag.Diagnostics
	client := m.(*rapi.Client)
	instances, err := client.GetInstancesBulk()
	if err != nil {
		return diag.FromErr(err)
	}

	if err = d.Set("instances", instances); err != nil {
		return diag.FromErr(err)
	}
	return diags;
}
